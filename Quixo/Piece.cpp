#include "Piece.h"

Piece::Piece() :
	Piece(Side::None)
{
	std::cout << "The Piece was created!" << std::endl;
}

Piece::Piece(Side side) :
	m_side(side)
{

}

/*Piece::Piece(const Piece & other)
{
	*this = other;
}

Piece::Piece(Piece && other)
{
	*this = std::move(other);
}

Piece & Piece::operator=(const Piece& other)
{
	m_side = other.m_side;
	return *this;
}
*/
Piece::~Piece()
{
	m_side = Side::None;
	std::cout << "The Piece was destructed!" << std::endl;
}

Piece::Side Piece::GetSide(Side side) const
{
	return side;
}

std::ostream& operator<<(std::ostream& os, const Piece & piece)
{
	return os << piece;
}

std::istream & operator>>(std::istream & is, const Piece & piece)
{
	return is >> piece;
}