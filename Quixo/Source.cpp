#include "QuixoGame.h"
#include <iostream>
#include "../Logging/Logging.h"
#include <fstream>

int main()
{
	std::ofstream of("syslog.log", std::ios::app);
	Logger logger(of);

	logger.log("Started Application...", Logger::Level::Info);

	QuixoGame quixoGame;
	quixoGame.Run();
	
	system("pause");
	return 0;
}