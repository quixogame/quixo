#include "stdafx.h"
#include "CppUnitTest.h"

#include "../QUiask/Player.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestProject
{
	TEST_CLASS(PlayerTests)
	{
		TEST_METHOD(defaultConstructorTest)
		{
			
			Player player("x");
			
			Assert::IsTrue(player.m_name == "x");

		}
	};

}
