#include "Player.h"
#include "Moves.h"
#include <string>

Player::Player(const std::string& name) :
	m_name(name)
{

}

GameBoard Player::PutPieceOnBoard(Piece::Side& piece, GameBoard& gameBoard, EdgePieces& edgePieces) const
{
	int option;
	Moves moves;
	GameBoard::Position position;
	position = edgePieces.ChoosePosition();
	std::cout << "Your option must be: \n  1(move up) \n  2(move down) \n  3(move to the right) \n  4(move to the left) \nPlease enter your option:";
	std::cin >> option;

	switch (option) {
	case 1:
		std::cout << "You chose to put the Piece up." << std::endl;
		moves.moveUp(position, gameBoard, piece);
		break;
	case 2:
		std::cout << "You chose to put the Piece down." << std::endl;
		moves.moveDown(position, gameBoard, piece);
		break;
	case 3:
		std::cout << "You chose to put the Piece on the right." << std::endl;
		moves.moveRight(position, gameBoard, piece);
		break;
	case 4:
		std::cout << "You chose to put the Piece on the left." << std::endl;
		moves.moveLeft(position, gameBoard, piece);
		break;
	default:
		break;
	}

	return gameBoard;
}

std::ostream& operator<<(std::ostream& os, const Player& player)
{
	return os << player.m_name;
}