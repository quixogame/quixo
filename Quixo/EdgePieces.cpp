#include "EdgePieces.h"

EdgePieces::EdgePieces()
{
}


bool EdgePieces::check(int coordX, int coordY, std::vector<std::pair<int, int>> vectorOfPositions)
{
	for (unsigned int index = 0; index < vectorOfPositions.size(); index++) 
	{
		if (coordX == vectorOfPositions[index].first && coordY == vectorOfPositions[index].second)
			return true;
	}
	return false;
}

GameBoard::Position EdgePieces::ChoosePosition()
{
	int coordX, coordY;
	bool ok = false;

	std::cout << "The line (coordinataX) is: ";
	std::cin >> coordX;
	std::cout << "The column (coordonataY) is: ";
	std::cin >> coordY;
	
	std::vector<std::pair<int, int>> vectorOfPositions;

	GameBoard::Position position = std::make_pair(coordX, coordY);

	vectorOfPositions.push_back(std::make_pair(0, 0));
	vectorOfPositions.push_back(std::make_pair(0, 1));
	vectorOfPositions.push_back(std::make_pair(0, 2));
	vectorOfPositions.push_back(std::make_pair(0, 3));
	vectorOfPositions.push_back(std::make_pair(0, 4));
	vectorOfPositions.push_back(std::make_pair(1, 0));
	vectorOfPositions.push_back(std::make_pair(1, 4));
	vectorOfPositions.push_back(std::make_pair(2, 0));
	vectorOfPositions.push_back(std::make_pair(2, 4));
	vectorOfPositions.push_back(std::make_pair(3, 0));
	vectorOfPositions.push_back(std::make_pair(3, 4));
	vectorOfPositions.push_back(std::make_pair(4, 0));
	vectorOfPositions.push_back(std::make_pair(4, 1));
	vectorOfPositions.push_back(std::make_pair(4, 2));
	vectorOfPositions.push_back(std::make_pair(4, 3));
	vectorOfPositions.push_back(std::make_pair(4, 4));

	ok = check(coordX, coordY, vectorOfPositions);
	while (ok == false)
	{
		std::cout << "You have to choose another Position because the Position you chose is NOT on the margin of the game board."<<std::endl;
		std::cout << "The line (coordinataX) is: ";
		std::cin >> coordX;
		std::cout << "The column (coordonataY) is: ";
		std::cin >> coordY;
		position = std::make_pair(coordX, coordY);
		ok = check(coordX, coordY, vectorOfPositions);
	}

	return position;
}