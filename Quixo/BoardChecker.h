#pragma once
#include "GameBoard.h"

class BoardChecker
{
public:
	enum State
	{
		None,
		WonFirstPlayer,
		WonSecondPlayer
	};
public:
	static State CheckGameBoard(const GameBoard& gameBoard);
};