#include "stdafx.h"
#include "CppUnitTest.h"
#include "../QUiask/GameBoard.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestProject
{
	TEST_CLASS(GameBoardTests)
	{
	public:

		TEST_METHOD(defaultConstructorEmptyBoard)
		{

			GameBoard gameBoard;

			for (int i = 0; i < GameBoard::numberOfLines; ++i)
				for (int j = 0; j < GameBoard::numberOfColumns; ++j)
					Assert::IsTrue(gameBoard.board[i][j]);

		}

		TEST_METHOD(SetGetAtOneOne)
		{
			GameBoard board;
			Piece piece;
			Piece::Side side;
			GameBoard::Position position{ 1, 1 };
			board[position] = piece;

			Assert::IsTrue(board[position].has_value());

			// Solution 1
			int memDiff = memcmp(&board[position], &piece, sizeof piece);
			Assert::AreEqual(0, memDiff);

			// Solution 2
			Assert::IsTrue(
				board[position]->GetSide(side) == piece.GetSide(side)
				
			);

			// Solution 3
			// overload operator == for Piece
			// and use Assert::AreEqual(*board[position], piece)
		}

		TEST_METHOD(GetAtOneOneConst)
		{
			const GameBoard gameBoard;
			GameBoard::Position position{ 1, 1 };

			Assert::IsFalse(gameBoard.board[1][1]);
		}

		TEST_METHOD(GetAtMinusOneOne)
		{
			GameBoard gameBoard;
			GameBoard::Position position{ -1, 1 };

			Assert::ExpectException<const char*>([&]() {
				gameBoard.board[-1][1];
			});
		}

		TEST_METHOD(GetAtMinusOneOneConst)
		{
			const GameBoard gameBoard;
			GameBoard::Position position{ -1, 1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				gameBoard.board[-1][1];
			});
		}

		TEST_METHOD(GetAtOneMinusOne)
		{
			GameBoard board;
			GameBoard::Position position{ 1, -1 };

			Assert::ExpectException<const char*>([&]() {
				board[position];
			});
		}

		TEST_METHOD(GetAtOneMinusOneConst)
		{
			const GameBoard gameBoard;
			GameBoard::Position position{ 1, -1 };

			Assert::ExpectException<std::out_of_range>([&]() {
				gameBoard.board[1][-1];
			});
		}
	    
	};


}
