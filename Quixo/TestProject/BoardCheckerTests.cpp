#include "stdafx.h"
#include "CppUnitTest.h"


#include "../QUiask/BoardChecker.h"

using namespace Microsoft::VisualStudio::CppUnitTestFramework;

namespace TestProject
{
	TEST_CLASS(BoardCheckerTests)
	{
	public:
		TEST_METHOD(NoneOnePiece)
		{
			GameBoard board;
			GameBoard::Position lastPosition{ 2, 1 };
			board[lastPosition] = Piece(Piece::Side::None);

			Assert::IsTrue(BoardChecker::CheckGameBoard(board, lastPosition) == BoardChecker::State::None);
		}
TEST_METHOD(WinMainDiagonalFirstPlayer)
		{
			GameBoard board;
			board[0][0] = board[1][1] = board[2][2] = board[3][3] = board[4][4] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardStateChecker::Check(board) == BoardStateChecker::State::WonFirstPlayer);
		}

		TEST_METHOD(WinMainDiagonalSecondPlayer)
		{
			GameBoard board;
			board[0][0] = board[1][1] = board[2][2] = board[3][3] = board[4][4] = Piece(Piece::Side::sideO);

			Assert::IsTrue(BoardStateChecker::Check(board) == BoardStateChecker::State::WonSecondPlayer);
		}

		TEST_METHOD(WinSecondaryDiagonalSecondPlayer)
		{
			GameBoard board;
			board[0][4] = board[1][3] = board[2][2] = board[3][1] = board[4][0] = Piece(Piece::Side::sideO);

			Assert::IsTrue(BoardStateChecker::Check(board) == BoardStateChecker::State::WonSecondPlayer);
		}

		TEST_METHOD(WinSecondaryDiagonalFirstPlayer)
		{
			GameBoard board;
			board[0][4] = board[1][3] = board[2][2] = board[3][1] = board[4][0] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardStateChecker::Check(board) == BoardStateChecker::State::WonFirstPlayer);
		}

		TEST_METHOD(WinLineOneFirstPlayer)
		{
			GameBoard board;
			board[0][0] = Piece(Piece::Side::sideX);
			board[0][1] = Piece(Piece::Side::sideX);
			board[0][2] = Piece(Piece::Side::sideX);
			board[0][3] = Piece(Piece::Side::sideX);
			board[0][4] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonFirstPlayer);
		}
		
		TEST_METHOD(WinLineTwoSecondPlayer)
		{
			GameBoard board;
			board[1][0] = Piece(Piece::Side::sideO);
			board[1][1] = Piece(Piece::Side::sideO);
			board[1][2] = Piece(Piece::Side::sideO);
			board[1][3] = Piece(Piece::Side::sideO);
			board[1][4] = Piece(Piece::Side::sideO);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonSecondPlayer);
		}

		TEST_METHOD(WinColumnThreeFirstPlayer)
		{
			GameBoard board;
			board[0][2] = Piece(Piece::Side::sideX);
			board[1][2] = Piece(Piece::Side::sideX);
			board[2][2] = Piece(Piece::Side::sideX);
			board[3][2] = Piece(Piece::Side::sideX);
			board[4][2] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonFirstPlayer);
		}

		TEST_METHOD(WinColumnFourSecondPlayer)
		{
			GameBoard board;
			board[3][0] = Piece(Piece::Side::sideO);
			board[3][1] = Piece(Piece::Side::sideO);
			board[3][2] = Piece(Piece::Side::sideO);
			board[3][3] = Piece(Piece::Side::sideO);
			board[3][4] = Piece(Piece::Side::sideO);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonSecondPlayer);
		}

		TEST_METHOD(WinColumnFiveFirstPlayer)
		{
			GameBoard board;
			board[0][4] = Piece(Piece::Side::sideX);
			board[1][4] = Piece(Piece::Side::sideX);
			board[2][4] = Piece(Piece::Side::sideX);
			board[3][4] = Piece(Piece::Side::sideX);
			board[4][4] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonFirstPlayer);
		}

		TEST_METHOD(WinLineThreeFirstPlayer)
		{
			GameBoard board;
			board[2][0] = Piece(Piece::Side::sideX);
			board[2][1] = Piece(Piece::Side::sideX);
			board[2][2] = Piece(Piece::Side::sideX);
			board[2][3] = Piece(Piece::Side::sideX);
			board[2][4] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonFirstPlayer);
		}

		TEST_METHOD(WinLineFourSecondPlayer)
		{
			GameBoard board;
			board[3][0] = Piece(Piece::Side::sideO);
			board[3][1] = Piece(Piece::Side::sideO);
			board[3][2] = Piece(Piece::Side::sideO);
			board[3][3] = Piece(Piece::Side::sideO);
			board[3][4] = Piece(Piece::Side::sideO);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonSecondPlayer);
		}
	
		TEST_METHOD(WinLineFiveFirstPlayer)
		{
			GameBoard board;
			board[4][0] = Piece(Piece::Side::sideX);
			board[4][1] = Piece(Piece::Side::sideX);
			board[4][2] = Piece(Piece::Side::sideX);
			board[4][3] = Piece(Piece::Side::sideX);
			board[4][4] = Piece(Piece::Side::sideX);

			Assert::IsTrue(BoardChecker::Check(board) == BoardChecker::State::WonFirstPlayer);
		}
		
	};
}
