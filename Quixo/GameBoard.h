#pragma once
#include "Piece.h"
#include <array>
#include <optional>

class GameBoard
{
public:
	using Position = std::pair<int, int>;

public:
	GameBoard() = default;

	//std::optional<Piece>& operator[] (const Position& pos);
	//friend std::ostream& operator << (std::ostream& os, const GameBoard& board);

	void operator = (const GameBoard& board);

public:
	static const size_t numberOfColumns = 5;
	static const size_t numberOfLines = 5;
	static const size_t boardSize = numberOfLines * numberOfColumns;

public:
	std::array<std::optional<Piece>, boardSize> m_pieces;


public:
	char board[5][5];

public:
	void showGameboard();

};